<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0MetaModule;

use R1c0MetaModule\Options\ModuleOptions;
use R1c0MetaModule\Service\MetaService;
use R1c0MetaModule\Entity\Meta;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventInterface;
use Zend\Http\Request as HttpRequest;
use Zend\Mvc\MvcEvent;

class Module implements BootstrapListenerInterface, AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface
{
	public function onBootstrap( EventInterface $event )
	{
		if( $event->getRequest() instanceof HttpRequest )
		{
			$eventManager = $event->getApplication()->getEventManager();
			$eventManager->attach( MvcEvent::EVENT_DISPATCH, array( $this, 'onDispatch' ) );
			$eventManager->attach( MvcEvent::EVENT_RENDER, array( $this, 'onRender' ) );
		}
	}
	
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php',
			),
		);
	}
	
	public function getConfig()
	{
		return require_once( __DIR__ . '/config/module.config.php' );
	}
	
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'R1c0MetaModule\Options\ModuleOptions' => function( ServiceLocatorInterface $serviceLocator ) {
					
					$moduleOptions = new ModuleOptions();
					$mergedConfig = $serviceLocator->get( 'Config' );
					
					if( isset( $mergedConfig['r1c0']['meta'] ) )
					{
						$moduleOptions->setFromArray( $mergedConfig['r1c0']['meta'] );
					}
					
					return $moduleOptions;
				},
				'R1c0MetaModule\Service\MetaService' => function( ServiceLocatorInterface $serviceLocator ) {
					
					$moduleOptions = $serviceLocator->get( 'R1c0MetaModule\Options\ModuleOptions' );
					$metaService = new MetaService(
						new Meta( $moduleOptions->getDefault() )
					);
					
					return $metaService;
				},
			),
		);
	}
	
	public function onDispatch( EventInterface $event )
	{
		$serviceLocator = $event->getApplication()->getServiceManager();
		$moduleOptions = $serviceLocator->get( 'R1c0MetaModule\Options\ModuleOptions' );
		$metaService = $serviceLocator->get( 'R1c0MetaModule\Service\MetaService' );
		
		foreach( $moduleOptions->getRoutes() as $route )
		{
			$metaService->addMeta( new Meta( $route ) );
		}
		
		if( $moduleOptions->getDatabaseEnabled() )
		{
			$entityManager = $serviceLocator->get( 'Doctrine\ORM\EntityManager' );
			$metaRepository = $entityManager->getRepository( 'R1c0MetaModule\Entity\Meta' );
			
			foreach( $metaRepository->findAll() as $meta )
			{
				$metaService->addMeta( $meta );
			}
		}
	}
	
	public function onRender( EventInterface $event )
	{
		$serviceLocator = $event->getApplication()->getServiceManager();
		$metaService = $serviceLocator->get( 'R1c0MetaModule\Service\MetaService' );
		$metaService->assign();
	}
}
