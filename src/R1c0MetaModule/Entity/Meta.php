<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0MetaModule\Entity;

use R1c0BaseModule\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="r1c0_meta")
 */
class Meta extends AbstractEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	protected $title;
	
	/**
	 * @ORM\Column(name="description", type="string", length=255)
	 */
	protected $description;
	
	/**
	 * @ORM\Column(name="keywords", type="string", length=255)
	 */
	protected $keywords;
	
	/**
	 * @ORM\Column(name="route", type="string", length=255)
	 */
	protected $route;
	
	/**
	 * @ORM\Column(name="params", type="json_array", nullable=true)
	 */
	protected $params;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setTitle( $title )
	{
		$this->title = $title;
		
		return $this;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setDescription( $description )
	{
		$this->description = $description;
		
		return $this;
	}
	
	public function getDescription()
	{
		return $this->description;
	}
	
	public function setKeywords( $keywords )
	{
		$this->keywords = $keywords;
		
		return $this;
	}
	
	public function getKeywords()
	{
		return $this->keywords;
	}
	
	public function setRoute( $route, array $params = null )
	{
		if( $params !== null )
		{
			$this->setParams( $params );
		}
		
		$this->route = $route;
		
		return $this;
	}
	
	public function getRoute()
	{
		return $this->route;
	}
	
	public function setParams( array $params = null )
	{
		$this->params = $params;
		
		return $this;
	}
	
	public function getParams()
	{
		return $this->params;
	}
}
