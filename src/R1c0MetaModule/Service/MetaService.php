<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0MetaModule\Service;

use R1c0MetaModule\Service\MetaServiceInterface;
use R1c0MetaModule\Entity\Meta;
use R1c0BaseModule\Service\AbstractService;
use Zend\View\Helper\Placeholder\Container\AbstractContainer;

class MetaService extends AbstractService implements MetaServiceInterface
{
	protected $default;
	
	protected $titleAttachOrder = AbstractContainer::SET;
	
	protected $metas = array();
	
	public function __construct( Meta $meta )
	{
		$this->setDefault( $meta );
	}
	
	public function setDefault( Meta $meta )
	{
		$this->default = $meta;
		
		return $this;
	}
	
	public function getDefault()
	{
		return $this->default;
	}
	
	public function setTitleAttachOrder( $setType )
	{
		$this->titleAttachOrder = $setType;
		
		return $this;
	}
	
	public function getTitleAttachOrder()
	{
		return $this->titleAttachOrder;
	}
	
	public function addMeta( Meta $meta )
	{
		$this->metas[] = $meta;
		
		return $this;
	}
	
	public function getMetas()
	{
		return $this->metas;
	}
	
	public function assign()
	{
		$serviceLocator = $this->getServiceLocator();
		
		$request = $serviceLocator->get( 'Request' );
		$router = $serviceLocator->get( 'Router' );
		
		if( $routeMatch = $router->match( $request ) )
		{
			$metas = $this->getMetas();
			
			usort( $metas, function( $a, $b ) {
				
				$paramsA = $a->getParams();
				$paramsB = $b->getParams();
				
				$issetA = isset( $paramsA['params'] );
				$issetB = isset( $paramsB['params'] );
				
				if( ( !$issetA ) && ( !$issetB ) )
				{
					return 0;
				}
				
				if( ( $issetA ) && ( $issetB ) )
				{
					$sizeA = sizeof( $paramsA['params'] );
					$sizeB = sizeof( $paramsB['params'] );
					
					if( $sizeA > $sizeB )
					{
						return -1;
					}
					
					if( $sizeA < $sizeB )
					{
						return 1;
					}
					
					return 0;
				}
				
				if( ( $issetA ) && ( !$issetB ) )
				{
					return -1;
				}
				
				return 1;
			});
			
			foreach( $metas as $meta )
			{
				if( $params = $meta->getParams() )
				{
					if( $meta->getRoute() == $routeMatch->getMatchedRouteName() )
					{
						$intersect = array_intersect_assoc( $params, $routeMatch->getParams() );
						
						if( sizeof( $params ) == sizeof( $intersect ) )
						{
							$this->setDefault( $meta );
							
							break;
						}
					}
				}
				else
				{
					if( $meta->getRoute() == $routeMatch->getMatchedRouteName() )
					{
						$this->setDefault( $meta );
						
						break;
					}
				}
			}
		}
		
		$viewHelperManager = $serviceLocator->get( 'ViewHelperManager' );
		
		$headTitle = $viewHelperManager->get( 'headtitle' );
		$headTitle->setTranslatorTextDomain( 'r1c0/meta' );
		$headTitle(
			$this->getDefault()->getTitle(),
			$this->getTitleAttachOrder()
		);
		
		$mvcTranslator = $serviceLocator->get( 'MvcTranslator' );
		$headMeta = $viewHelperManager->get( 'headmeta' );
		
		if( $description = $this->getDefault()->getDescription() )
		{
			$headMeta->setName( 'description',
				$mvcTranslator->translate( $description, 'r1c0/meta' )
			);
		}
		
		if( $keywords = $this->getDefault()->getKeywords() )
		{
			$headMeta->setName( 'keywords',
				$mvcTranslator->translate( $keywords, 'r1c0/meta' )
			);
		}
	}
}
