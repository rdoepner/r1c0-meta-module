-- -----------------------------------------------------
-- Table `r1c0_meta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_meta`;

CREATE TABLE IF NOT EXISTS `r1c0_meta` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `keywords` VARCHAR(255) NOT NULL,
  `route` VARCHAR(255) NOT NULL,
  `params` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table data `r1c0_meta`
-- -----------------------------------------------------
INSERT INTO `r1c0_meta` (`id`, `title`, `description`, `keywords`, `route`, `params`) VALUES
(1, 'Carousel', '', '', 'page', '{"root":"carousel"}'),
(2, 'Contact', '', '', 'contact', NULL);
